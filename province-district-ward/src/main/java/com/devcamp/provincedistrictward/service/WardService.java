package com.devcamp.provincedistrictward.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.repository.IWardRepository;

@Service
public class WardService {
  @Autowired
  IWardRepository pIWardRepository;

  public ArrayList<Ward> getAllWards() {
    ArrayList<Ward> wards = new ArrayList<>();
    pIWardRepository.findAll().forEach(wards::add);
    return wards;
  }
}
