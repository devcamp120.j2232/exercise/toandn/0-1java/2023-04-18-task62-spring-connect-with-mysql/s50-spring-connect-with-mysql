package com.devcamp.provincedistrictward.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Province;
import com.devcamp.provincedistrictward.repository.IProvinceRepository;

@Service
public class ProvinceService {
  @Autowired
  IProvinceRepository pIProvinceRepository;

  public ArrayList<Province> getAllProvinces() {
    ArrayList<Province> provinces = new ArrayList<>();
    pIProvinceRepository.findAll().forEach(provinces::add);
    return provinces;
  }

  public Set<District> getDistrictByProvinceId(int id) {
    Province vProvince = pIProvinceRepository.findById(id);
    if (vProvince != null) {
      return vProvince.getDistricts();
    } else {
      return null;
    }
  }

}
