package com.devcamp.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincedistrictward.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Integer> {

}
