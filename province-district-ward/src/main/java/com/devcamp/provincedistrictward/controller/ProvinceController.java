package com.devcamp.provincedistrictward.controller;

import java.util.*;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.support.Repositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Province;
import com.devcamp.provincedistrictward.repository.IProvinceRepository;
import com.devcamp.provincedistrictward.service.DistrictService;
import com.devcamp.provincedistrictward.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
  @Autowired
  ProvinceService provinceService;

  @Autowired
  IProvinceRepository pIProvinceRepository;

  @Autowired
  DistrictService districtService;

  @GetMapping("/devcamp-provinces")
  public ResponseEntity<List<Province>> getAllProvinces() {
    try {
      return new ResponseEntity<>(provinceService.getAllProvinces(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/devcamp-districts")
  public ResponseEntity<Set<District>> getDistrictByProvinceId(@RequestParam(value = "id") int id) {
    try {
      Set<District> vDistrict = provinceService.getDistrictByProvinceId(id);
      if (vDistrict != null) {
        return new ResponseEntity<>(vDistrict, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/province5")
  public ResponseEntity<List<Province>> getFiveProvince(
      @RequestParam(value = "page", defaultValue = "1") String page,
      @RequestParam(value = "size", defaultValue = "5") String size) {
    try {
      Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page),
          Integer.parseInt(size));
      List<Province> list = new ArrayList<Province>();
      pIProvinceRepository.findAll(pageWithFiveElements).forEach(list::add);
      return new ResponseEntity<>(list, HttpStatus.OK);
    } catch (Exception e) {
      return null;
    }
  }
}