package com.devcamp.provincedistrictward.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "provinces")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class Province {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(name = "code_province")
  private String codeProvince;

  @Column(name = "name_province")
  private String nameProvince;

  @OneToMany(mappedBy = "province", cascade = CascadeType.ALL)
  private Set<District> districts;

  public Province() {
  }

  public Province(int id, String codeProvince, String nameProvince) {
    this.id = id;
    this.codeProvince = codeProvince;
    this.nameProvince = nameProvince;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCodeProvince() {
    return codeProvince;
  }

  public void setCodeProvince(String codeProvince) {
    this.codeProvince = codeProvince;
  }

  public String getNameProvince() {
    return nameProvince;
  }

  public void setNameProvince(String nameProvince) {
    this.nameProvince = nameProvince;
  }

  public Set<District> getDistricts() {
    return districts;
  }

  public void setDistricts(Set<District> districts) {
    this.districts = districts;
  }

}
